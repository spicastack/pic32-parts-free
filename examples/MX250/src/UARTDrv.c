#include <UARTDrv.h>
#include <GPIODrv.h>
#include <system.h>
#include <interrupt.h>

// Init UART
// RX is RB13 aka. RPB13. Page 132 of manual says to set U1RXR<3:0> to 0011 for U1RX @ RPB13
// -> Shared with thermometer pin
// TX is RB3 aka. RPB3. Page 134 says to set RP*** to 0001, to set that pin as U1TX.
// -> Shared with LED1 on DP32 board
// Don't forget to set TRIS accordingly
// Set RX as pull-up - won't hurt. CNPUx register. 
// Pin On-change notifications need to be disabled (default)
// Fun fact, all I/O modules have CLR, SET and INV register for atomic bit-manipulation. Neat

volatile uint32_t count = 0;
uint8_t temp;

// For interrupts http://wiki.pinguino.cc/index.php/PIC32MX_Development#Using_CHANGE_NOTICE_interrupt
//U1RX – UART1 Receive Done IRQ40 Vector32 IFS1<8> IEC1<8> IPC8<4:2> IPC8<1:0>
//U1TX – UART1 Transfer Done IRQ41 Vector32 IFS1<9> IEC1<9> IPC8<4:2> IPC8<1:0>

// As defined in interrupt.h/c
INTERRUPT(UART1Interrupt){
	// Should check if TX or RX interrupt Just RX for now.
	count++;		// Increase counter, for our test
	temp = U1RXREG;	// Readout data, otherwise we'll be stuck here
	IFS1CLR = 1<<8;	// Clear bits. TODO nicer. 
}


void UARTDrv_Init(uint32_t baud){
	U1MODEbits.ON = 0;

	UART_TX_ANSELbits.UART_TX_ANSELBIT = 0;	// Disable analog functionality
	UART_TX_TRISbits.UART_TX_TRISPIN = 0;	// 0 == output
	UART_TX_LATbits.UART_TX_LATPIN = 1;	// Set high, as UART is Idle High
	RPB3R = UART_TX_REMAP;					// Set to which pin

	UART_RX_ANSELbits.UART_RX_ANSELBIT = 0;	// Disable analog fucntionality
	UART_RX_TRISbits.UART_TX_TRISPIN = 1;	// 1 == input
	UART_RX_CNPUbits.UART_RX_CNPUBIT = 1;	// Enable pull-up
	U1RXR = UART_RX_REMAP;					// Set to which pin
	
	U1MODEbits.SIDL = 0;	// Stop when in IDLE mode
	U1MODEbits.IREN	= 0;	// Disable IrDA
	U1MODEbits.RTSMD = 0;	// Don't care, RTS not used
	U1MODEbits.UEN = 0;		// TX & RX controlled by UART peripheral, RTC & CTS are not.
	U1MODEbits.WAKE = 0;	// Don't wake up from sleep
	U1MODEbits.LPBACK = 0;	// Loopback mode disabled
	U1MODEbits.ABAUD = 0;	// No autobauding
	U1MODEbits.RXINV = 0;	// Idle HIGH
	U1MODEbits.BRGH = 0;	// Standard speed mode - 16x baud clock
	U1MODEbits.PDSEL = 0;	// 8 bits, no parity
	U1MODEbits.STSEL = 0;	// 1 stop bit

	U1STAbits.ADM_EN = 0;	// Don't care for auto address detection, unused
	U1STAbits.ADDR = 0;		// Don't care for auto address mark
	U1STAbits.UTXISEL = 00;	// Generate interrupt, when at least one space available (unused)
	U1STAbits.UTXINV = 0;	// Idle HIGH
	U1STAbits.URXEN = 1;	// UART receiver pin enabled
	U1STAbits.UTXBRK = 0;	// Don't send breaks.
	U1STAbits.UTXEN = 1;	// Uart transmitter pin enabled
	U1STAbits.URXISEL = 0;	// Interrupt what receiver buffer not empty
	U1STAbits.ADDEN = 0;	// Address detect mode disabled (unused)
	U1STAbits.OERR = 0;		// Clear RX Overrun bit - not important at this point

	//U1BRG = 12;			// Should calculate as (PBCLK/BRGH?4:16)/BAUD - 1
	U1BRG = (GetPeripheralClock() / (U1MODEbits.BRGH ? 4 : 16)) / baud - 1;

	// New - setup interrupt
	IPC8bits.U1IP = 1;		// Priorty = 1 (one above disabled)
	IPC8bits.U1IS = 0;		// Subpriority = 0 (least)
	IEC1bits.U1RXIE = 1;	// Enable interrupt	

	U1MODEbits.ON = 1;
}

void UARTDrv_SendBlocking(uint8_t * buffer, uint32_t length){
	
	uint32_t counter = 0;

	for (counter = 0; counter<length; counter++){
		while(U1STAbits.UTXBF){ asm("nop"); }
		U1TXREG = buffer[counter];
	}

	// Wait until sent
	while(U1STAbits.TRMT){
		asm("nop");
	}
}

uint32_t UARTDrv_GetCount(){
	return count;
}


