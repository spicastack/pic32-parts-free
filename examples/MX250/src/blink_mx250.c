#include <p32xxxx.h>    // Always in first place to avoid conflict with const.h -> this includes xc.h
// Deleted Pinguino includes - not relevant for barebone example
#include <system.h>     // System setup
#include <const.h>      // MIPS32
#include <inttypes.h>
//#include <delay.c>      // Alright, can use this


// Newlib / sprintf related (see TODO below)
#include <string.h>
#include <stdio.h>
#include <newlib.h>
#include <errno.h>

// Drivers for HW
#include <GPIODrv.h>
#include <UARTDrv.h>
#include <BTNDrv.h>

// Config bits. Used to be defined with #pragma xyz in XC32
const uint32_t __attribute__((section (".SECTION_DEVCFG3"))) temp3 = 0xcfffffff;   // DEVCFG3
const uint32_t __attribute__((section (".SECTION_DEVCFG2"))) temp2 = 0xfff979d9;   // DEVCFG2
const uint32_t __attribute__((section (".SECTION_DEVCFG1"))) temp1 = 0xff6a0d5b;   // DEVCFG1
const uint32_t __attribute__((section (".SECTION_DEVCFG0"))) temp0 = 0x7ffffffb;   // DEVCFG0

char tempArray[128];
uint8_t lengthArray;


// TODO in main.h
void * sbrk(uint32_t extraMem);

// TODO - run the timer, like in MX440 example (SysTick style)
void simpleDelay(unsigned int noOfLoops){
    unsigned int i = 0;
    while (i<noOfLoops){
        i++;
        asm("nop");
    }
}

void setup(){
	// What is the equivalent of SYSTEMConfigPerformance?
	// -> Setting up the system for the required System Clock
	// -> Seting up the Wait States
	// -> Setting up PBCLK
	// -> Setting up Cache module (not presenf on MX2*, but is on MX4)
	// Also of interest: https://microchipdeveloper.com/32bit:mx-arch-exceptions-processor-initialization
	// See Pic32 reference manual, for CP0 info http://ww1.microchip.com/downloads/en/devicedoc/61113e.pdf


	// To setup KSEG0 (cacheable are), do this:
	unsigned int val;
	val = _CP0_GET_CONFIG();	// Macro present in cp0defs.h 
	val |= 0x00000003;	// Sets to KSEG0/cachable region, as per Table 2-11 in ref. manual
	_mtc0(_CP0_CONFIG, _CP0_CONFIG_SELECT, val);	// No macro, do it as suggested in first link.

	BMXCONbits.BMXWSDRM = 0;	// Set wait-states to 0
	
	// System config, call with desired CPU freq. and PBCLK divisor
	SystemConfig(48000000L, 2);	// Can't go to 50MHz - 8MHz / 2 * 12 = 48MHz max.

	UARTDrv_Init(115200);

	BTNDrv_Init();

	// Enable interrupts
	INTEnableSystemMultiVectoredInt();
}

int main()
{
    // Make sure JTAG stays enabled, when doing JTAG (later)
    
	setup();
	simpleDelay(4000000);	// Small delay after setup, so the UART line goes IDLE (reduces garbage)
	// TODO - Move to LEDDrv
    // Our LED is on RB3, aka AN5 - we need to disable that analog input
    LED2_ANSELbits.LED2_ANSELBIT = 0;   // Disable analog input
    LED2_TRISbits.LED2_TRISPIN = 0;   // Set as output
    LED2_LATbits.LED2_LATPIN = 1;	// Set to SHINE

    for(;;){
		lengthArray = snprintf(tempArray, 128, "Hello from MX250, running GCC :)\n");
		UARTDrv_SendBlocking((uint8_t *)tempArray, lengthArray);

		if (BTNDrv_GetButtonState()){
			lengthArray = snprintf(tempArray, 128, "Button pressed!\n");
			UARTDrv_SendBlocking((uint8_t *)tempArray, lengthArray);
		}

		// TODO - malloc doesn't work
		// -> Consequently, snprintf with numbers (like below) won't work.
		//lengthArray = snprintf(tempArray, 128, "Number of RX'd chars: %lu\n", 1);
		//tempMalloc = malloc(64);
		//UARTDrv_SendBlocking((uint8_t *)tempMalloc, 64);
		//sbrk(0);
	
        simpleDelay(4000000);
		simpleDelay(4000000);
		simpleDelay(4000000);
		simpleDelay(4000000);

		LED2_LATINV = LED2_Mask;	// Invert LED output

    }

/**********************************************************************/

    return(0);
    
} // end of main

/*
// https://stackoverflow.com/questions/27290086/gcc-canaries-undefined-reference-to-stack-chk-guard
unsigned long __stack_chk_guard;
void __stack_chk_guard_setup(void){
     __stack_chk_guard = 0xBAAAAAAD;	// Provide some magic numbers
}

void __stack_chk_fail(void){
	// Error message                                 
}	// Will be called when guard variable is corrupted


void * sbrk(uint32_t extraMem){

	extern uint8_t _heap_head;	// In linker
	extern uint8_t _heap_end;	// Also in linker

	static uint8_t * heap_head = 0;
	uint8_t * old_heap_head;



	if (heap_head == 0){
		heap_head = &_heap_head;
	}

	old_heap_head = heap_head;

	if ((heap_head + extraMem) > &_heap_end){
		// FYI, code execution will be blocked if you get here.
		errno = ENOMEM;
		return (void *) -1;
	}

	heap_head = heap_head + extraMem;
	return (void *) old_heap_head;
}

*/

