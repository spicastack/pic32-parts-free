#include <p32xxxx.h> 
#include <const.h>  
#include <inttypes.h>
#include <BTNDrv.h>
#include <GPIODrv.h>



void BTNDrv_Init(){
	BTN2_TRISbits.BTN2_TRISPIN = 1;	// Set pin to INPUT
	// Button has external pull-down. Idle LOW


}

uint8_t BTNDrv_GetButtonState(void){
	if (BTN2_PORTbits.BTN2_PORTPIN == 1){
		return 1;
	}
	else{
		return 0;
	}
}
