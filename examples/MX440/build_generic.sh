#!/bin/bash
set -e
set -x

source "./$1_defs"

INCLUDES="-I./include -I../../include"
FLAGS="-march=m4k -EL -msoft-float -nostdlib -static -Os  -ffunction-sections -fdata-sections -Wl,--gc-sections -Wl,--defsym,_min_heap_size=512 -Wl,-Map=./output.map -D __PIC32MX__ -D __32MX440F256H__ -D PIC32_PINGUINO_MICRO -lm -lgcc -lc -Werror -Wall -fdollars-in-identifiers $INCLUDES $DEFS"

"$GCC_PREFIX"-gcc -march=m4k -EL -msoft-float -nostdlib -static -Os  -ffunction-sections -fdata-sections -Wl,--gc-sections -Wl,--defsym,_min_heap_size=512 -Wl,-Map=./output.map -D __PIC32MX__ -D __32MX440F256H__ -D PIC32_PINGUINO_MICRO -lm -lgcc -lc -fdollars-in-identifiers $INCLUDES -c -o $NAME.o $NAME.c $DEFS

"$GCC_PREFIX"-gcc -march=m4k -EL -msoft-float -nostdlib -static -Os  -ffunction-sections -fdata-sections -Wl,--gc-sections -Wl,--defsym,_min_heap_size=512 -Wl,-Map=./output.map -D __PIC32MX__ -D __32MX440F256H__ -D PIC32_PINGUINO_MICRO -lm -lgcc -lc -fdollars-in-identifiers $INCLUDES -S -o $NAME.s $NAME.c $DEFS

"$GCC_PREFIX"-gcc ${FLAGS} -c -o processor.o ../../proc/32MX440F256H/p32MX440F256H.S

"$GCC_PREFIX"-gcc ${FLAGS} -c -o isrwrapper.o ./lib/isrwrapper.c

"$GCC_PREFIX"-gcc -march=m4k -EL -msoft-float -nostdlib -static -Os  -ffunction-sections -fdata-sections -Wl,--gc-sections -Wl,--defsym,_min_heap_size=512 -Wl,-Map=./output.map_link -D __PIC32MX__ -D __32MX440F256H__ -D PIC32_PINGUINO_MICRO -T../../proc/32MX440F256H/procdefs_pinguino.ld -T../../ldscripts/elf32pic32mx.ld -fdollars-in-identifiers $INCLUDES ./$NAME.o ../../libpic32/startup/crt0.S ../../libpic32/startup/general-exception.S ./processor.o ./ISRwrapper.S ./isrwrapper.o -Wl,-undefined,dynamic_lookup -lm -lgcc -lc -o ./$NAME.elf $DEFS

"$GCC_PREFIX"-objcopy -O ihex ./$NAME.elf ./$NAME.hex
