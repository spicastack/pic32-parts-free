#ifndef GPIODRV_H_b89c70aecf4d4e88a69c4f9db0e8fa35
#define GPIODRV_H_b89c70aecf4d4e88a69c4f9db0e8fa35


#define UART_TX_TRISbits	TRISDbits
#define UART_TX_TRISPIN		TRISD11
#define UART_TX_LATbits		LATDbits
#define UART_TX_LATPIN		LATD11
#define UART_TX_REMAP		0b0001	// Write to RP***

#define UART_RX_TRISbits	TRISDbits
#define UART_RX_TRISPIN		TRISD10
#define UART_RX_CNPUbits	CNPUDbits
#define UART_RX_CNPUBIT		CNPUD10
#define UART_RX_REMAP		0b0011	// Write to U1RXR

#define LED_TRISbits		TRISHbits
#define LED_TRISPIN			TRISH2
#define LED_LATbits			LATHbits
#define LED_LATPIN			LATH2
#define LED_LATINV			LATHINV
#define LED_Mask			(1<<2)

#define BTN_ANSELbits	ANSELBbits
#define BTN_ANSELBIT	ANSB12
#define BTN_TRISbits	TRISBbits
#define BTN_TRISPIN		TRISB12
#define BTN_PORTbits	PORTBbits
#define BTN_PORTPIN		RB12


#endif
