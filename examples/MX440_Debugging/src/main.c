#include <p32xxxx.h>    // Always in first place to avoid conflict with const.h -> this includes xc.h
// Deleted Pinguino includes - not relevant for barebone example
#include <system.h>     // System setup
#include <const.h>      // MIPS32
#include <inttypes.h>
//#include <delay.c>      // Alright, can use this


// Newlib / sprintf related (see TODO below)
#include <string.h>
#include <stdio.h>
#include <newlib.h>
#include <errno.h>

// Drivers for HW
//#include <GPIODrv.h>
//#include <UARTDrv.h>
//#include <BTNDrv.h>

// Config bits. Used to befined with #pragma xyz in XC32

const uint32_t __attribute__((section (".SECTION_DEVCFG3"))) temp3 = 0xFFFFBEEF;   // DEVCFG3
const uint32_t __attribute__((section (".SECTION_DEVCFG2"))) temp2 =
	0xFFF87888
	| (0b000<<_DEVCFG2_FPLLODIV_POSITION)
	| (0b0<<_DEVCFG2_UPLLEN_POSITION)
	| (0b001<<_DEVCFG2_UPLLIDIV_POSITION)
	| (0b101<<_DEVCFG2_FPLLMUL_POSITION)
	| (0b001<<_DEVCFG2_FPLLIDIV_POSITION);   // DEVCFG2
const uint32_t __attribute__((section (".SECTION_DEVCFG1"))) temp1 =
	0xFF600858
	| (0b10100<<_DEVCFG1_WDTPS_POSITION)
	| (0b01<<_DEVCFG1_FPBDIV_POSITION)
	| (0b0<<_DEVCFG1_OSCIOFNC_POSITION)
	| (0b10<<_DEVCFG1_POSCMOD_POSITION)
	| (0b0<<_DEVCFG1_IESO_POSITION)	//?
	| (0b0<<_DEVCFG1_FSOSCEN_POSITION)
	| (0b011<<_DEVCFG1_FNOSC_POSITION);   // DEVCFG1
const uint32_t __attribute__((section (".SECTION_DEVCFG0"))) temp0 =
	0x6EF00FF4 // There's that _one_ bit that has to be 0
	| (0b1<<_DEVCFG0_CP_POSITION)
	| (0b1<<_DEVCFG0_BWP_POSITION)
	| (0b11111111<<_DEVCFG0_PWP_POSITION)
	| (0b1<<_DEVCFG0_ICESEL_POSITION)	// PGEC2/PGED2
#ifdef DEBUG_BUILD	// Defined with Makefile
	| (0b10<<_DEVCFG0_DEBUG_POSITION);   // DEVCFG0
#else
	| (0b11<<_DEVCFG0_DEBUG_POSITION);   // DEVCFG0	- DEBUG NEEDS TO BE DISABLED, IF NO DEBUGGER PRESENT! Otherwise code doesn't run.
#endif

volatile char tempArray[128];
volatile uint8_t lengthArray = 0;

// TODO - run the timer, like in MX440 example (SysTick style)
void simpleDelay(unsigned int noOfLoops){
    unsigned int i = 0;
    while (i<noOfLoops){
        i++;
        asm("nop");
    }
}

void setup(){
	// What is the equivalent of SYSTEMConfigPerformance?
	// -> Setting up the system for the required System Clock
	// -> Seting up the Wait States
	// -> Setting up PBCLK
	// -> Setting up Cache module (not presenf on MX2*, but is on MX4)
	// Also of interest: https://microchipdeveloper.com/32bit:mx-arch-exceptions-processor-initialization
	// See Pic32 reference manual, for CP0 info http://ww1.microchip.com/downloads/en/devicedoc/61113e.pdf

	// To setup KSEG0 (cacheable are), do this:
	unsigned int val;
	val = _CP0_GET_CONFIG();	// Macro present in cp0defs.h 
	val |= 0x00000003;	// Sets to KSEG0/cachable region, as per Table 2-11 in ref. manual
	_mtc0(_CP0_CONFIG, _CP0_CONFIG_SELECT, val);	// No macro, do it as suggested in first link.

	BMXCONbits.BMXWSDRM = 0;	// Set wait-states to 0
	
	// System config, call with desired CPU freq. and PBCLK divisor
	SystemConfig(80000000L, 2);	// Try the 80MHz. Set PBCLK to half, test at 80 later?

	//UARTDrv_Init(115200);

	//BTNDrv_Init();

	// Enable interrupts
	INTEnableSystemMultiVectoredInt();
}

int main(){

	setup();

/*
	int* p = (int*)0xBF8860C0;
	*p = 0;
	p = (int*) 0xBF8860E0;
	*p = 0xFFFFFFFF;	
	for(;;);
*/
	// In this chip, to set/unset ANSEL bits, use the AD1PCFG register
	
	TRISDbits.TRISD1 = 0;	// Set to output
	LATDbits.LATD1 = 1;		// Turn on

	//TRISGbits.TRISG6 = 0;
	//LATGbits.LATG6 = 1;	

    for(;;){
    	simpleDelay(4000000);
    	simpleDelay(4000000);
		simpleDelay(4000000);
		simpleDelay(4000000);
		LATDINV = (1<<1);

    }

/**********************************************************************/

    return(0);
    
} // end of main


