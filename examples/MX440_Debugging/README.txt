This is an example program for the PIC32-PINGUINO-MICRO dev board,
intended to show how to setup JTAG debugging.

The program performs the minimal requirements to use the microcontroller,
which is setup the hardware, and blink an LED.

It shows how to setup configurations bits properly and how to
automate that via the Makefile.

In the wiki, under "Debugging", you can checkout how to setup an IDE,
and perform all the steps to setup, compile, flash, and debug the program.
