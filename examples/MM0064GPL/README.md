PIC32MM GPL example project
===========================

This is an example program for the PIC32MM Curiosity Development board
[DM320101](https://www.microchip.com/DevelopmentTools/ProductDetails/DM320101), that has a [PIC32MM0064GPL036](https://www.microchip.com/wwwproducts/en/PIC32MM0064GPL036) on it.

## Pins used
The following pins are used or reserved by the example program
- LEDs
  - RA3, Red LED, active HIGH
  - RB12, Green LED, active HIGH
  - RA2, Blue LED, active HIGH
- Buttons
  - RB7, active LOW
  - RB13, active LOW
- UART
  - U1TX -> RB14
  - U1RX -> RB15
- ICSP (on-board programmer)
  - PGED3 -> RB5
  - PGEC3 -> RB6 
- ICSP (external programmer)
  - PGED2 -> RB10
  - PGEC2 -> RB11
- JTAG
  - TCK -> RB8
  - TMS -> RB9
  - TDI -> RB11 (shared with ICSP)
  - TDO -> RB10 (shared with ICSP)

## Program overview
The following things are tested with this example program:
- GPIO (LED writing, Button reading)
- UART (blocking TX at 115200bps, RX interrupt)
- Interrupts (see UART RX interrupt)
- sprintf test with integers and floats

The example prints out several lines around 4 times per second, each containing some information:
- Greeting and a loop counter
- Status of both buttons and number of received bytes
- A floating point printout

Additionaly, depending on the number of received bytes in the UART RX interrupt routine, a different combination of LEDs is lit.

Note that for testing floating point number in sprintf, it is necessary to include the proper link option in the Makefile -> `-u _printf_float`. If you don't require floating point support in printing, leave that out, as the size difference is quite huge (cca. extra 20kB).

The printout should look something like this:
>Hello from PIC32MM! Counter = 0
BTN1: 0, BTN2: 0, RXd bytes: 0
Float test: 0.100000
Hello from PIC32MM! Counter = 1
BTN1: 0, BTN2: 0, RXd bytes: 0
Float test: 0.414000
Hello from PIC32MM! Counter = 2
BTN1: 0, BTN2: 0, RXd bytes: 0
Float test: 0.728000
...


## On-board programmer
The on-board Pickit3 programmer is not used, as it is not supported by the Microchip's scripting firmware, and as such doesn't work with [progyon](https://gitlab.com/spicastack/progyon). It should still work with the manufacturers own tools, but YMMV.

Instead, the pins outlined in the "Pins used" section can be used for ICSP and/or JTAG programming with an external adapter.

Note, if you wish to completely disable the on-board Pickit3, you can solder a jumper
from its MCLR to GND on headers J1 or J7.
