This is an example program for the an MX270 dev board,
setup so that debugging via JTAG is possible.

The setup has been tested on a RedFoxBoard 
( https://github.com/Neofoxx/RedFoxBoard )
